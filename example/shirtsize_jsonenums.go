// generated by jsonenums -type=ShirtSize; DO NOT EDIT
package main

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
)

func init() {
	// stubs for imports
	_ = json.Delim('s')
	_ = driver.Int32

}

var ErrShirtSizeInvalid = errors.New("ShirtSize is invalid")

func init() {
	var v ShirtSize
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_ShirtSizeNameToValue = map[string]ShirtSize{
			interface{}(NA).(fmt.Stringer).String(): NA,
			interface{}(XS).(fmt.Stringer).String(): XS,
			interface{}(S).(fmt.Stringer).String():  S,
			interface{}(M).(fmt.Stringer).String():  M,
			interface{}(L).(fmt.Stringer).String():  L,
			interface{}(XL).(fmt.Stringer).String(): XL,
		}
	}
}

var _ShirtSizeNameToValue = map[string]ShirtSize{
	"NA": NA,
	"XS": XS,
	"S":  S,
	"M":  M,
	"L":  L,
	"XL": XL,
}

var _ShirtSizeValueToName = map[ShirtSize]string{
	NA: "NA",
	XS: "XS",
	S:  "S",
	M:  "M",
	L:  "L",
	XL: "XL",
}

// String is generated so ShirtSize satisfies fmt.Stringer.
func (r ShirtSize) String() string {
	s, ok := _ShirtSizeValueToName[r]
	if !ok {
		return fmt.Sprintf("ShirtSize(%d)", r)
	}
	return s
}

// Validate verifies that value is predefined for ShirtSize.
func (r ShirtSize) Validate() error {
	_, ok := _ShirtSizeValueToName[r]
	if !ok {
		return ErrShirtSizeInvalid
	}
	return nil
}

// MarshalJSON is generated so ShirtSize satisfies json.Marshaler.
func (r ShirtSize) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _ShirtSizeValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid ShirtSize: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so ShirtSize satisfies json.Unmarshaler.
func (r *ShirtSize) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("ShirtSize should be a string, got %s", data)
	}
	v, ok := _ShirtSizeNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid ShirtSize %q", s)
	}
	*r = v
	return nil
}

func (t *ShirtSize) Scan(src interface{}) error {
	i, ok := src.(int64)
	if !ok {
		return fmt.Errorf("can't scan from %T", src)
	}
	*t = ShirtSize(i)
	return nil
}

func (t ShirtSize) Value() (driver.Value, error) {
	return int64(t), nil
}

func ShirtSizeFromString(src string) ShirtSize {
	return _ShirtSizeNameToValue[src]
}

func (t *ShirtSize) UnmarshalText(src []byte) error {
	v, ok := _ShirtSizeNameToValue[string(src)]
	if !ok {
		return fmt.Errorf("invalid ShirtSize %q", string(src))
	}
	*t = v
	return nil
}
