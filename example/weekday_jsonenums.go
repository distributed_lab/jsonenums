// generated by jsonenums -type=WeekDay; DO NOT EDIT
package main

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
)

func init() {
	// stubs for imports
	_ = json.Delim('s')
	_ = driver.Int32

}

var ErrWeekDayInvalid = errors.New("WeekDay is invalid")

func init() {
	var v WeekDay
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_WeekDayNameToValue = map[string]WeekDay{
			interface{}(Monday).(fmt.Stringer).String():    Monday,
			interface{}(Tuesday).(fmt.Stringer).String():   Tuesday,
			interface{}(Wednesday).(fmt.Stringer).String(): Wednesday,
			interface{}(Thursday).(fmt.Stringer).String():  Thursday,
			interface{}(Friday).(fmt.Stringer).String():    Friday,
			interface{}(Saturday).(fmt.Stringer).String():  Saturday,
			interface{}(Sunday).(fmt.Stringer).String():    Sunday,
		}
	}
}

var _WeekDayNameToValue = map[string]WeekDay{
	"Monday":    Monday,
	"Tuesday":   Tuesday,
	"Wednesday": Wednesday,
	"Thursday":  Thursday,
	"Friday":    Friday,
	"Saturday":  Saturday,
	"Sunday":    Sunday,
}

var _WeekDayValueToName = map[WeekDay]string{
	Monday:    "Monday",
	Tuesday:   "Tuesday",
	Wednesday: "Wednesday",
	Thursday:  "Thursday",
	Friday:    "Friday",
	Saturday:  "Saturday",
	Sunday:    "Sunday",
}

// Validate verifies that value is predefined for WeekDay.
func (r WeekDay) Validate() error {
	_, ok := _WeekDayValueToName[r]
	if !ok {
		return ErrWeekDayInvalid
	}
	return nil
}

// MarshalJSON is generated so WeekDay satisfies json.Marshaler.
func (r WeekDay) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _WeekDayValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid WeekDay: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so WeekDay satisfies json.Unmarshaler.
func (r *WeekDay) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("WeekDay should be a string, got %s", data)
	}
	v, ok := _WeekDayNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid WeekDay %q", s)
	}
	*r = v
	return nil
}

func (t *WeekDay) Scan(src interface{}) error {
	i, ok := src.(int64)
	if !ok {
		return fmt.Errorf("can't scan from %T", src)
	}
	*t = WeekDay(i)
	return nil
}

func (t WeekDay) Value() (driver.Value, error) {
	return int64(t), nil
}

func WeekDayFromString(src string) WeekDay {
	return _WeekDayNameToValue[src]
}

func (t *WeekDay) UnmarshalText(src []byte) error {
	v, ok := _WeekDayNameToValue[string(src)]
	if !ok {
		return fmt.Errorf("invalid WeekDay %q", string(src))
	}
	*t = v
	return nil
}
